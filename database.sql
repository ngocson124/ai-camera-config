-- MariaDB dump 10.17  Distrib 10.4.6-MariaDB, for osx10.14 (x86_64)
--
-- Host: 10.193.79.164    Database: smart_camera
-- ------------------------------------------------------
-- Server version	10.3.9-MariaDB-1:10.3.9+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ai_backends`
--

DROP TABLE IF EXISTS `ai_backends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ai_backends` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ma` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mota` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loai_ia` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Loại AI backend: giao_thong, face',
  `ketnoi` smallint(6) NOT NULL DEFAULT 0 COMMENT 'Tình trạng kết nối: 1 = đang kết nối; 0 = mất kết nối',
  `hoatdong` smallint(6) NOT NULL DEFAULT 0 COMMENT 'Kích hoạt: 1 = Đã kích hoạt; 0 = Chưa kích hoạt',
  `daxoa` smallint(6) NOT NULL DEFAULT 0 COMMENT 'Đã xóa: 1 = Đã xóa; 0 = Chưa xóa',
  `nguoitao` bigint(20) unsigned NOT NULL,
  `last_update_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ai_backends_ma_unique` (`ma`),
  KEY `ai_backends_nguoitao_foreign` (`nguoitao`),
  CONSTRAINT `ai_backends_nguoitao_foreign` FOREIGN KEY (`nguoitao`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ai_backends`
--

LOCK TABLES `ai_backends` WRITE;
/*!40000 ALTER TABLE `ai_backends` DISABLE KEYS */;
/*!40000 ALTER TABLE `ai_backends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ioc_cmr_areas`
--

DROP TABLE IF EXISTS `ioc_cmr_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ioc_cmr_areas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ma` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cap` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `daxoa` tinyint(4) NOT NULL DEFAULT 0,
  `nguoitao` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nguoisua` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ioc_cmr_areas`
--

LOCK TABLES `ioc_cmr_areas` WRITE;
/*!40000 ALTER TABLE `ioc_cmr_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ioc_cmr_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ioc_cmr_cameras`
--

DROP TABLE IF EXISTS `ioc_cmr_cameras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ioc_cmr_cameras` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ma` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vrc_id` bigint(20) unsigned DEFAULT NULL,
  `code_service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `long` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `luonggoc` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `areid` int(11) DEFAULT NULL,
  `roadid` int(11) DEFAULT NULL,
  `typeid` int(11) DEFAULT NULL,
  `ketnoi` tinyint(4) NOT NULL DEFAULT 0,
  `phienban` int(11) NOT NULL DEFAULT 1,
  `nguoitao` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nguoisua` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ioc_cmr_cameras_vrc_id_foreign` (`vrc_id`),
  CONSTRAINT `ioc_cmr_cameras_vrc_id_foreign` FOREIGN KEY (`vrc_id`) REFERENCES `video_receiving_clients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ioc_cmr_cameras`
--

LOCK TABLES `ioc_cmr_cameras` WRITE;
/*!40000 ALTER TABLE `ioc_cmr_cameras` DISABLE KEYS */;
/*!40000 ALTER TABLE `ioc_cmr_cameras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ioc_cmr_cameratypes`
--

DROP TABLE IF EXISTS `ioc_cmr_cameratypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ioc_cmr_cameratypes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ma` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phienban` int(11) DEFAULT 1,
  `daxoa` tinyint(4) NOT NULL DEFAULT 0,
  `nguoitao` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nguoisua` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ioc_cmr_cameratypes`
--

LOCK TABLES `ioc_cmr_cameratypes` WRITE;
/*!40000 ALTER TABLE `ioc_cmr_cameratypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ioc_cmr_cameratypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ioc_cmr_roads`
--

DROP TABLE IF EXISTS `ioc_cmr_roads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ioc_cmr_roads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `areas_id` bigint(20) unsigned NOT NULL,
  `ma` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `daxoa` tinyint(4) DEFAULT 1,
  `nguoitao` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nguoisua` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ioc_cmr_roads_areas_id_foreign` (`areas_id`),
  CONSTRAINT `ioc_cmr_roads_areas_id_foreign` FOREIGN KEY (`areas_id`) REFERENCES `ioc_cmr_areas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ioc_cmr_roads`
--

LOCK TABLES `ioc_cmr_roads` WRITE;
/*!40000 ALTER TABLE `ioc_cmr_roads` DISABLE KEYS */;
/*!40000 ALTER TABLE `ioc_cmr_roads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager_services`
--

DROP TABLE IF EXISTS `manager_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mota` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager_services`
--

LOCK TABLES `manager_services` WRITE;
/*!40000 ALTER TABLE `manager_services` DISABLE KEYS */;
INSERT INTO `manager_services` VALUES (5,'SI-IOC','SI-IOC','SI-IOC','2019-12-10 02:32:30','2019-12-13 17:00:21','6af3add6-4023-4185-abfe-17b6aaa16b0f');
/*!40000 ALTER TABLE `manager_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule_faces`
--

DROP TABLE IF EXISTS `rule_faces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule_faces` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ma` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_camera` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_zone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_ai_backend` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_rule` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'LOẠI AI XỬ LÝ;100: face',
  `ketnoi` smallint(6) NOT NULL DEFAULT 0,
  `hoatdong` smallint(6) NOT NULL DEFAULT 0 COMMENT 'Kích hoạt: 1 = Đã kích hoạt; 0 = Chưa kích hoạt',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `threshold` float DEFAULT NULL,
  `use_vrc_rtsp` smallint(6) DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rule_faces_ma_unique` (`ma`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule_faces`
--

LOCK TABLES `rule_faces` WRITE;
/*!40000 ALTER TABLE `rule_faces` DISABLE KEYS */;
/*!40000 ALTER TABLE `rule_faces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule_giaothong`
--

DROP TABLE IF EXISTS `rule_giaothong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule_giaothong` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ma` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_camera` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_ai_backend` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_rule` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'LOẠI AI XỬ LÝ;0 => lưu lượng 1 => phát hiện biển số 2 => Vi phạm hiệu lệnh của đèn tín hiệu giao thông 3 => Vi phạm làn đường 4 => Vi phạm dừng đỗ sai quy định',
  `restream` smallint(6) DEFAULT 0 COMMENT '1 => ENABLE TÍNH NĂNG RESTREAM',
  `frame_coordinate` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Vùng cần bắt',
  `stop_coordinate` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Vùng dừng đèn đỏ',
  `traffic_light` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tọa độ đèn giao thông',
  `traffic_light_left` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `traffic_light_right` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `define_lanes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'TOẠ ĐỘ các vùng vi phạm làn đường',
  `non_parking_lanes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Vùng cấm đỗ xe',
  `hoatdong` smallint(6) DEFAULT 0 COMMENT 'Kích hoạt: 1 = Đã kích hoạt; 0 = Chưa kích hoạt',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `trafic_light_right_status` smallint(6) DEFAULT 0,
  `prohibited_vehicles` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detect_crowd_area` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detect_human_area` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detect_weapon_area` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `crow_threshold` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `use_vrc_rtsp` smallint(6) DEFAULT 1,
  `stream_uri` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rule_giaothong_ma_unique` (`ma`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule_giaothong`
--

LOCK TABLES `rule_giaothong` WRITE;
/*!40000 ALTER TABLE `rule_giaothong` DISABLE KEYS */;
/*!40000 ALTER TABLE `rule_giaothong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'IOC','ioc@vnpt.vn',NULL,'$2y$10$dHO755mZafipQw1oNAWXQe27gSbFeclus268yQHm0NlXQttH2XVnG','4eZCuFzYFq7pXGG7kXFOrF8qgVQJk9wYtf1v8nnTHfrd4zxgSeFZHGmZxkAw','2019-12-10 02:23:42','2019-12-10 02:23:42'),(4,'Bùi Thái Trường','buithaitruong@gmail.com',NULL,'$2y$10$LuMrjR4erT2ZWVdWTehtkeBorefSqlKMJ4uQoZc8n83osXsGMuBbS',NULL,'2019-12-12 10:09:09','2019-12-12 10:09:09');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_receiving_clients`
--

DROP TABLE IF EXISTS `video_receiving_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_receiving_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ma` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mota` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ketnoi` tinyint(4) DEFAULT NULL,
  `hoatdong` tinyint(4) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ip` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port_admin` int(11) NOT NULL,
  `port_stream` int(11) NOT NULL,
  `app_stream` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cdn_resource` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`ma`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_receiving_clients`
--

LOCK TABLES `video_receiving_clients` WRITE;
/*!40000 ALTER TABLE `video_receiving_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_receiving_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrc_service`
--

DROP TABLE IF EXISTS `vrc_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vrc_service` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vrc_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrc_service`
--

LOCK TABLES `vrc_service` WRITE;
/*!40000 ALTER TABLE `vrc_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrc_service` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-05 16:57:32
